#include "gpio.h"

#include "raii.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;


template <typename T>
void GPIO::writeOnce(const string& location, T data) {
    ofstream f(location.c_str());
    raii::Closer<ofstream&> closer = raii::Closer<ofstream&>{f};
    f << data;
}


template <typename T>
T GPIO::readOnce(const string& location) {
    ifstream f(location.c_str());
    auto closer = raii::Closer<ifstream&>{f};
    T result;
    f >> result;
    return result;
}


Pin
GPIO::getPin(const string& p) {
    writeOnce("/sys/class/gpio/export", p);
    return Pin{p, this};
}


int Pin::get() {
    return fGPIO->readOnce<int>(string("/sys/class/gpio/gpio" + fPin + "/value"));
}

void Pin::set(int val) {
    fGPIO->writeOnce(string("/sys/class/gpio/gpio" + fPin + "/value"), val);
}


void Pin::setDir(const string& dir) {
    fGPIO->writeOnce(string("/sys/class/gpio/gpio" + fPin + "/direction"), dir);
}

void Pin::unexport() {
    fGPIO->writeOnce("/sys/class/gpio/unexport", fPin);
}
