namespace raii {
    template <typename T>
    struct Closer {
        T closeMe;

        ~Closer() {closeMe.close();}
    };
};
