
blinker: blinker.cpp gpio.h gpio.cpp
	g++ -Wall -std=c++0x blinker.cpp gpio.cpp -g -o blinker

button: button.cpp gpio.h gpio.cpp
	g++ -Wall -std=c++0x button.cpp gpio.cpp -g -o button
