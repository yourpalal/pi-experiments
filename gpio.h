#ifndef __GPIO_H
#define __GPIO_H


#include <string>


class GPIO;

class Pin {
public:
    int get();
    void set(int);

    void setDir(const std::string& dir);
    void unexport();

private:

    friend class GPIO;

    Pin(const std::string& p, GPIO* g) : fPin(p), fGPIO(g) {};

    std::string fPin;
    GPIO*  fGPIO;
};


class GPIO {
public:
    Pin getPin(const std::string& p);

private:
    friend class Pin;

    template <typename T>
    void writeOnce(const std::string& location, T data);

    template <typename T>
    T readOnce(const std::string& location);
};


#endif
