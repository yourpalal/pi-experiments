/* written with reference to https://www.ridgerun.com/wiki/index.php?title=Gpiopin.cpp */

#include "gpio.h"

using namespace std;

int main() {
    auto gpio = GPIO{};
    auto pin = gpio.getPin("16");
    pin.setDir("out");

    for (int i = 0; i < 100; i++) {
        pin.set(i % 2);
        usleep(1000000);
    }
    pin.unexport();
};
