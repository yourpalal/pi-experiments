/* written with reference to https://www.ridgerun.com/wiki/index.php?title=Gpiopin.cpp */

#include <iostream>

using namespace std;

#include "gpio.h"


int main() {
    GPIO gpio;
    auto pin = gpio.getPin("21");
    pin.setDir("in");

    auto val = pin.get();
    cout << val << endl;

    while (val == pin.get()) {
        usleep(1000000);
        cout << ".";
        cout.flush();
    }
    cout << endl;
    pin.unexport();
};
